# NFDI4Ing conference

This is the GitLab Repository for the NFDI4Ing confernce.

Each year has its own subfolder, starting with the year 2022 hosted by RWTH Aachen.

This years conference can be found on [this link](https://nfdi4ing.de/conference/).

| Year    | Host | Website |
| ----------- | ----------- | ----------- |
| 2021     | TU Braunschweig       | [Link](https://nfdi4ing.de/events/konferenz_2021/) |
| 2022   | RWTH Aachen        | [Link](https://nfdi4ing.de/conference-2022/) |
| 2023   | TU and SLUB Dresden       | [Link](https://nfdi4ing.de/conference/) |
| 2024   | ...        | [Link]() |
| 2025   | ...       | [Link]() |


## License
All files in this Repository are licensed under a MIT license (code) or CC-BY 4.0 licence (everything else) if not stated otherwise.

# Acknowledgements

The authors would like to thank the Federal Government and the Heads of Government of the Länder, as well as the Joint Science Conference (GWK), for their funding and support within the framework of the NFDI4Ing consortium. Funded by the German Research Foundation (DFG) - project number 442146713.

