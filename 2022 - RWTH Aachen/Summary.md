# Summary of the NFDI4Ing Conference 2022 hosted by RWTH Aachen University

The [NFDI4Ing conference 2022 "Unifying the Understanding of RDM in Engineering Science"](https://nfdi4ing.de/conference-2022/) took place on October 26 and 27, 2022.

Beforehand, the [CC-41 Community Meeting](https://nfdi4ing.de/events/communitymeeting_cc-41_2022/) at 03.03.2022 was used to gain some expertise on how such an event could be carried out in NFDI4Ing.

After that, the planning of the conference started immediately. 

With 297 registrations and a concurrent count of approximately 120 participants. 

On the first day, 1532 logins or changes between meeting rooms happened during the conference with the average time spent in a room beeing 29 minutes.
178 different participants have at least joined the meeting once, spending a total of 244 minutes in the conference in average.

![Users on the 26.10.2022](img/20221026_Users.png)

On the second day, 1247 logins or changes between meeting rooms happened during the conference with the average time spent in a room beeing 32 minutes.
158 different participants have at least joined the meeting once, spending a total of 253 minutes in the conference in average.

![Users on the 27.10.2022](img/20221027_Users.png)

## Registration development

A big leap could be identified at the ent of august, which may be connected to the postings on the group feeds of research data alliance. Below a selection of advertising measurements can be found:
- July 2022: [WZL LinkedIn Post](https://www.linkedin.com/posts/profile-areas-of-rwth-aachen-university_research-data-rdm-activity-6958357950625112065-vJ14/?utm_source=share&utm_medium=member_desktop)
- 01.07.2022: Mail to NFDI4Ing participants, information and gesamtteam mail distribution list
- 05.08.2022: Mail to NFDI-all, RWTH Fachbereiche, RDM-Initiatives and others mail distribution lists
- 22.08.2022: [Group Metadata IG, Education And Training On Handling Of Research Data IG, Research Data Management In Engineering IG](https://www.rd-alliance.org/group/metadata-ig-education-and-training-handling-research-data-ig-research-data-management)
- 22.08.2022: [Group Discipline-Specific Guidance For Data Management Plans WG](https://www.rd-alliance.org/group/discipline-specific-guidance-data-management-plans-wg/post/last-call-nfdi4ing-call)
- 22.08.2022: [Group Research Data Management In Engineering IG ](https://www.rd-alliance.org/group/research-data-management-engineering-ig/post/last-call-nfdi4ing-call-contributions-nfdi4ing)
- 26.09.2022: [News on NFDI4Ing website](https://nfdi4ing.de/4-6/)
- Oktober 2022: [WZL LinkedIn Post](https://www.linkedin.com/posts/werkzeugmaschinenlabor-wzl-der-rwth-aachen_datamanagement-engineering-activity-6989152856163377152-teAf?utm_source=share&utm_medium=member_desktop)
- 14.10.2022: Mail to NFDI section infeng mail distribution list
- 17.10.2022: Mail to NFDI4Ing participants, information and gesamtteam mail distribution list
- 25.10.2022: re3data invites their colleagues to the conference

![Registration](img/Registrations.png)

## How to conference

After setting up a project charta, the scope of the conference was defined as well as the topic "Unifying the Understanding of RDM in Engineering Science". Multiple workshops were held to set these key conditions for the conference. The results of these workshops can be found [here](https://git.rwth-aachen.de/nfdi4ing/nfdi4ing-conference/-/blob/main/2022%20-%20RWTH%20Aachen/01_Planung/20221118_Konferenzplanung-v1.0.0.pdf). A data management plan was developed of which a cut version without confidential information can now be found on [this GitLab](https://git.rwth-aachen.de/nfdi4ing/nfdi4ing-conference/-/blob/main/2022%20-%20RWTH%20Aachen/01_Planung/NFDI4Ing%20conference%202022.xml) as well. Keep in mind: This .xml file is missing plenty of information on purpose as internal data is not supposed to be openly availiable.

The next big step was setting up the website along with the subpages:

- [General conference website](https://nfdi4ing.de/conference-2022/)
- [Abstract submission website](https://nfdi4ing.de/conference_abstract_submission/) (Now: Active redirect to General website)
- [Abstract website](https://nfdi4ing.de/conference_abstracts/)
- [Review submission website](https://nfdi4ing.de/conference_review_submission/)
- [Jury review website](https://nfdi4ing.de/conference_jury_submission/)

Also the Call for Contributions was designed and can be found and reused [here](https://git.rwth-aachen.de/nfdi4ing/nfdi4ing-conference/-/tree/main/2022%20-%20RWTH%20Aachen/02_Call_for_contributions).

Following that, a process was set up to partly automate the communitcation as well as the data processing of the coordination between organizing committee and the contributors. The results of this are [Jupyter Notebooks](https://git.rwth-aachen.de/nfdi4ing/nfdi4ing-conference/-/tree/main/2022%20-%20RWTH%20Aachen/09_Automatisierung), one for each process step in the conference organisation. Also, a template table for the data to be processed can be found as well. It is suggested to fit the first file of the scripts to use the .csv exported by Elementor (The Base of the Websites). This task however is given to the next years Organizing Comittee. 

Also, some python scripts were so commonly used, that they went to the `config` directory in the afforementiones Jupyter folder, so they could be called from there.

## A RDM perspective
Regarding RDM, a [RDMO data management plan](https://git.rwth-aachen.de/nfdi4ing/nfdi4ing-conference/-/blob/main/2022%20-%20RWTH%20Aachen/01_Planung/NFDI4Ing%20conference%202022.xml) has been set up as well as a [coscine project](https://coscine.rwth-aachen.de/p/nfdi4ing-conference-2022/). Also, there is a [Zenodo publication](https://doi.org/10.5281/zenodo.7362038) .

## The Time Machine

### Website
While the [CC-41 Community Meetings](https://nfdi4ing.de/events/communitymeeting_cc-41_2022/) website could be reused, most of it had to be restructured to fit the desired design and functionality. Elementor offered a good basis to work on, however styling of text was quite tedious and using the default textblocks of Elementor and switching between HTML and Preview-View often resulted in inline HMTL configuraiton and CSS styling disappearing. Peace was never an option.

Therefore HMTL blocks have seen a massive amount of use on the website as they were the only reliable implementation to keep a style as intented. This goes for all HMTL and CSS functionallity that is different to the general design of the website. Also functionality in HMTL wont suddenly disappear with an Elementor update. This does not apply for anchors set with the Elementor anchor tool. Those may just stop working at some point and, as a add on, disable all HTML functionality that follows the anchor on the whole website, even the ones in HTML blocks.

> I, personally, really recommend to also implement anchors in HMTL blocks. Save you some trouble.

### Communication 
The communication in this project was massive. More than 2.100 Mails were received and send in context of the conference. Although many of those were automated, the communication with contributors, key note speakers as well as other partners like the NFDI4Ing Geschäftsstelle or the Jury took the most ammount of time. 


## What else?

Our lessons learned summarised:

- Think about a Pre-Registration for submission (notification that something is to be submitted, but without concrete designation of the topic or format; contact and institution only)
- With many of the names of speakers, it was unsure how to pronounce their names. Maybe it is an approach to ask for the pronunciation of names.
- Use breaks for technical checks with contributors or have one person do the tech-check in a seperate room the whole time, one contributor after another
- Start early! The call for contributions has to go out ASAP
- Dont be affraid to extend deadlines. It will yield you more contributions!

Also, we got plenty of Feedback in the end of the conference. Most participants perceived the conference as extremely well organized and well structured and with great support. Many explicitly thanked the organizing comittee for their great effort and engagement.

![I am happy with the conference and its presentations](img/1.png)

![Have your expectations regarding the conference been met?](img/2.png)

![Next time, I would like the conference to contain more about:](img/3.png)

![The format I enjoyed most was:](img/4.png)

![What did you like most about the NFDI4Ing conference 2022?](img/5.png)

![How can the conference be improved in the future?](img/6.png)


## Acknowledgement
The authors would like to thank the Federal Government and the Heads of Government of the Länder, as well as the Joint Science Conference (GWK), for their funding and support within the framework of the NFDI4Ing consortium. Funded by the German Research Foundation (DFG) - project number 442146713.



