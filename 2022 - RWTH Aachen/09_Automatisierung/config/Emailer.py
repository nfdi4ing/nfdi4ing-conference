"""
title: Emailer.py
topics: conference, nfdi4ing
author: Tobias Hamann
short_name: hmt
created: 2022-09-19
version: 0.1.0
last updated: 2022-09-19
edited by: Tobias Hamann
"""

# Define the Mailer Class
# Needs Outlook!
def Emailer(To_recipients, CC_recipients, BCC_recipients, subject, text, auto_send=False):
    import win32com.client as win32   

    outlook = win32.Dispatch('outlook.application')
    mail = outlook.CreateItem(0)
    mail.To = To_recipients
    mail.CC = CC_recipients
    mail.BCC = BCC_recipients
    mail.Subject = subject
    mail.HtmlBody = text
    if auto_send:
        mail.send
    else:
        mail.Display(True)    
