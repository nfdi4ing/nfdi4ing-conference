
"""
title: ToDoLister.ipynb
topics: nfdi4ing, nfdi, reth, konferenz, conference
author: Tobias Hamann
short_name: hmt
created: 2022-09-23
version: 0.1.0
last updated: 2022-09-23
edited by: one name, another name
"""


def ToDos_Lister(AddedToDo):
    import os


    # Compile File Name
    Dateiname="Konferenz_ToDos.txt"

    # Check if ToDos file exists
    if os.path.isfile(Dateiname):
        with open(Dateiname) as ToDos_txt:
            # Load txt-file
            lines = ToDos_txt.read()
    else:
        # Create empty config object
        lines="ToDo-Liste für die Konferenz: \n (Du kannst erledigte Aufgaben einfach löschen und die Datei dann speichern) \n"

    lines=lines+"\n- "+AddedToDo

    #Write ToDofile
    with open(Dateiname, "w") as ToDos_txt:
        # Write txt-file
        ToDos_txt.write(lines)

